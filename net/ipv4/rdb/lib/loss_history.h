/*
 * Implementation of Redundant Data Bundling (RDB) in TCP.
 *
 * Copyright (C) 2012-2015, Bendik Rønning Opstad <bro.devel+kernel@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * The TFRC code is based on the TFRC implemention from net/dccp/ccids/
 * and customized for RDB.
 */

#ifndef _RDB_LI_HIST_
#define _RDB_LI_HIST_

#include <linux/ktime.h>
#include <linux/list.h>
#include <linux/slab.h>

#include "rdb_tfrc.h"

/*
 * Number of loss intervals (RFC 4342, 8.6.1). The history size is one more than
 * NINTERVAL, since the `open' interval I_0 is always stored as the first entry.
 */
#define NINTERVAL	8
#define LIH_SIZE	(NINTERVAL + 1)

/**
 *  struct rdb_tfrc_loss_interval - Loss history record for TFRC-based protocols
 *  @li_seqno: Highest received seqno before the start of loss
 *  @li_mstamp: Timestamp of the first loss event
 *  @li_mstamp: Timestamp of the first loss event
 *  @li_length: Loss interval length
 *  @li_lost: Number of loss events in this interval
 *  @is_short: If this inetval is short
 */
struct rdb_tfrc_loss_interval {
	u64	li_seqno;
	ktime_t li_tstamp;
	struct skb_mstamp li_mstamp; // Presumably don't need both timestamps, so should remove one of them
	u32	li_length;
	u32 li_lost;
	bool is_short;
};

/**
 *  struct tfrc_loss_hist - Loss record history
 *  @ring: Circular queue managed in LIFO manner
 *  @counter: Current count of entries (can be more than %LIH_SIZE)
 *  @i_mean: Current Average Loss Interval [RFC 3448, 5.4]
 */
struct tfrc_loss_hist {
	struct rdb_tfrc_loss_interval *ring[LIH_SIZE];
	u8 counter;
	u32 i_mean;
};

static inline void tfrc_lh_init(struct tfrc_loss_hist *lh)
{
	memset(lh, 0, sizeof(struct tfrc_loss_hist));
}

static inline u8 tfrc_lh_is_initialised(struct tfrc_loss_hist *lh)
{
	return lh->counter > 0;
}

static inline u8 tfrc_lh_length(struct tfrc_loss_hist *lh)
{
	return min(lh->counter, (u8)LIH_SIZE);
}

struct tfrc_rx_hist;
extern u32 tfrc_lh_get_interval_len(struct tfrc_loss_hist *lh, const u8 i);
extern struct rdb_tfrc_loss_interval *tfrc_lh_get_interval(struct tfrc_loss_hist *lh, const u8 i);
extern int  tfrc_lh_interval_add(struct tfrc_loss_hist *, struct tfrc_rx_hist *,
								 u32 (*first_li)(struct sock *, struct rdb_tfrc *), struct sock *);
extern u8   tfrc_lh_update_i_mean(struct tfrc_loss_hist *lh, struct sk_buff *);
extern void tfrc_lh_cleanup(struct tfrc_loss_hist *lh);
extern struct rdb_tfrc_loss_interval *tfrc_lh_peek(struct tfrc_loss_hist *lh);
extern void tfrc_lh_calc_i_mean(struct tfrc_loss_hist *lh);
extern void tfrc_lh_calc_i_mean_sp(struct tfrc_loss_hist *lh, u32 rtt);
extern struct rdb_tfrc_loss_interval *tfrc_lh_demand_next(struct tfrc_loss_hist *lh);

extern int rdb_tfrc_interval_add(struct tfrc_loss_hist *lh, struct sk_buff *skb,
								 int lost_count, u32 rtt,
								 u32 (*calc_first_li)(struct sock *, struct rdb_tfrc *), struct sock *sk,
								 struct rdb_tfrc *tfrc);

extern int  rdb_tfrc_li_init(void);
extern void rdb_tfrc_li_exit(void);

#endif /* _RDB_LI_HIST_ */
