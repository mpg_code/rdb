/*
 * Implementation of Redundant Data Bundling (RDB) in TCP.
 *
 * Copyright (C) 2012-2015, Bendik Rønning Opstad <bro.devel+kernel@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <net/net_namespace.h>
#include <asm/unaligned.h>
#include <linux/tcp.h>
#include <linux/ftrace.h>
#include <net/tcp.h>

#include "rdb_cc.h"
#include "rdb_skb.h"
#include "lib/rdb_tfrc.h"

int dynamic_pif __read_mostly = 0;
module_param(dynamic_pif, int, 0644);
MODULE_PARM_DESC(dynamic_pif, "Dynamic PIF limit (as lowest allowed ITT in ms) for thin stream. 0 to disable (default=0)");

static const char procname[] = "rdb";

int sysctl_tcp_thin_packet_limit __read_mostly = 4;
int sysctl_tcp_rdb_max_bundle_bytes __read_mostly = TCP_RDB_MAX_BUNDLE_BYTES;
int sysctl_tcp_rdb_max_bundle_skbs __read_mostly = 0;
int sysctl_tcp_rdb_packet_count = 0;
int sysctl_tcp_rdb_bytes_bundled = 0;


static struct tcp_congestion_ops tcp_reno_3_15 __read_mostly = {
	.ssthresh	= tcp_reno_ssthresh,
	.cong_avoid	= tcp_reno_cong_avoid_3_15,
	.owner		= THIS_MODULE,
	.name		= "reno_3_15",
};

static struct tcp_congestion_ops tcp_rdb __read_mostly = {
	.init		= rdb_cong_init,
	.release    = rdb_cong_release,
	.ssthresh	= tcp_reno_ssthresh,
	.cong_avoid	= tcp_rdbcong_avoid,
	.cwnd_event	= tcp_rdb_event,
	.pkt_send   = do_rdb,
	.owner		= THIS_MODULE,
	.name		= "rdb",
};

static struct tcp_congestion_ops tcp_rdb_tfrc __read_mostly = {
	.init		= rdb_cong_tfrc_init,
	.release    = rdb_cong_tfrc_release,
	.ssthresh	= tcp_rdb_ssthresh,
	.cong_avoid	= tcp_rdbcong_avoid_tfrc,
	.cwnd_event	= tcp_rdb_event,
	.pkt_send   = do_rdb,
	.owner		= THIS_MODULE,
	.name		= "rdb_tfrc",
};


static struct ctl_table rdb_table[] = {
	{
		.procname       = "rdb_max_bundle_bytes",
		.data           = &sysctl_tcp_rdb_max_bundle_bytes,
		.maxlen         = sizeof(int),
		.mode           = 0644,
		.proc_handler   = &proc_dointvec
	},
	{
		.procname       = "rdb_max_bundle_skbs",
		.data           = &sysctl_tcp_rdb_max_bundle_skbs,
		.maxlen         = sizeof(int),
		.mode           = 0644,
		.proc_handler   = &proc_dointvec
	},
	{
		.procname       = "rdb_packet_count",
		.data           = &sysctl_tcp_rdb_packet_count,
		.maxlen         = sizeof(int),
		.mode           = 0644,
		.proc_handler   = &proc_dointvec
	},
	{
		.procname       = "rdb_bytes_bundled",
		.data           = &sysctl_tcp_rdb_bytes_bundled,
		.maxlen         = sizeof(int),
		.mode           = 0644,
		.proc_handler   = &proc_dointvec
	},
	{
		.procname       = "tcp_thin_packet_limit",
		.data           = &sysctl_tcp_thin_packet_limit,
		.maxlen         = sizeof(int),
		.mode           = 0644,
		.proc_handler   = &proc_dointvec
	},
	{ }
};

static struct ctl_table_header *rdb_table_header;


static int __init tcp_rdb_register(void)
{
	int ret = -ENOMEM;

	rdb_table_header = register_net_sysctl(&init_net, "net/ipv4/rdb", rdb_table);
	if (rdb_table_header == NULL) {
		pr_info("rdb_module failed to initialize sysctl header\n");
		goto out;
	}

	if (rdb_lib_init())
		goto undo_sysctl_table;

	if (tcp_register_congestion_control(&tcp_rdb)) {
		pr_info("tcp_rdb failed to register congestion control tcp_rdb\n");
		goto do_lib_exit;
	}

	if (tcp_register_congestion_control(&tcp_rdb_tfrc)) {
		pr_info("tcp_rdb failed to register congestion control tcp_rdb_tfrc\n");
		goto do_lib_exit;
	}

	if (tcp_register_congestion_control(&tcp_reno_3_15)) {
		pr_info("tcp_rdb failed to register congestion control tcp_reno_3_15\n");
		goto do_lib_exit;
	}

	ret = 0;
	pr_info("rdb_module setup successfully. Dynamic PIF: %d\n", dynamic_pif);
	goto out;

do_lib_exit:
	rdb_lib_exit();
undo_sysctl_table:
	unregister_net_sysctl_table(rdb_table_header);
out:
	return ret;
}

static void __exit tcp_rdb_unregister(void)
{
	unregister_net_sysctl_table(rdb_table_header);
	rdb_lib_exit();
	tcp_unregister_congestion_control(&tcp_rdb);
	tcp_unregister_congestion_control(&tcp_rdb_tfrc);
	tcp_unregister_congestion_control(&tcp_reno_3_15);
}

module_init(tcp_rdb_register);
module_exit(tcp_rdb_unregister);

MODULE_AUTHOR("Bendik Rønning Opstad");
MODULE_DESCRIPTION("TCP RDB");
MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");
